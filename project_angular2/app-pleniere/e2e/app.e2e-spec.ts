import { AppPlenierePage } from './app.po';

describe('app-pleniere App', () => {
  let page: AppPlenierePage;

  beforeEach(() => {
    page = new AppPlenierePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
