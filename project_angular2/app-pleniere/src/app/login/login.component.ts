import 'rxjs/add/operator/switchMap';
import { Component, OnInit }        from '@angular/core';
import {Router} from "@angular/router";
import {LoginService} from "./login.service";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.css' ]
})
export class LoginComponent implements OnInit {

  model: any = {};

  constructor(private loginService: LoginService, private router: Router) {}

  ngOnInit(): void {

  }

  login(): void {
    this.loginService.login(this.model.username, this.model.password)
      .subscribe(result => {
        this.router.navigate(['mortalite']);
      });
  }

}
