import { Http, BaseRequestOptions, Response, ResponseOptions, RequestMethod } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: Http,
  useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
    // configure fake backend
    backend.connections.subscribe((connection: MockConnection) => {
      let testUser = { username: 'eleveur', password: 'eleveur', role: 'eleveur', lastName: 'eleveur' };
      let testMortalite = {   age: 12,
        date: 20170304,
        sexe: 0,
        nbMort: 0,
        obs: "ras"
      };
      let testLivraisons = {
          "date": 20170304,
      "quantitee":20,
        "designation": "ras"
    };
      let testStock = {
        "lotPrecedent": 20170304,
        "totalConsomme": 12,
        "stockRestant": 20170304
      };

      let testAliment = {
        "value": 12
      };

      // wrap in timeout to simulate server api call
      setTimeout(() => {

        // fake authenticate api end point
        if (connection.request.url.endsWith('/api/v1.0/authenticate') && connection.request.method === RequestMethod.Post) {
          // get parameters from post request
          let params = JSON.parse(connection.request.getBody());

          // check user credentials and return fake jwt token if valid
          console.log(testUser.username);
          console.log(testUser.password);
          if (params.username === testUser.username && params.password === testUser.password) {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: {     token: "fake-jwt-token",
                role: "eleveur",
                resources: [
                  {
                    name: "r1",
                    description: "des",
                    actions: [
                      "READ",
                      "WRITE"
                    ]
                  }
                ]
              } })
            ));
          } else {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200 })
            ));
          }
        }

        if (connection.request.url.endsWith('/api/v1.0/subscribe') && connection.request.method === RequestMethod.Post) {
          // get parameters from post request
          let params = JSON.parse(connection.request.getBody());

          // check user credentials and return fake jwt token if valid
          console.log(testUser.username);
          console.log(testUser.password);
          if (params.username === testUser.username && params.password === testUser.password) {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: {     token: "fake-jwt-token",
                role: "eleveur",
                resources: [
                  {
                    name: "r1",
                    description: "des",
                    actions: [
                      "READ",
                      "WRITE"
                    ]
                  }
                ]
              } })
            ));
          } else {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200 })
            ));
          }
        }

        // fake subscribe api end point
        if (connection.request.url.endsWith('/api/v1.0/eleveurs/elevages/aliments/stock/consomme') && connection.request.method === RequestMethod.Post) {
          // get parameters from post request
          let params = JSON.parse(connection.request.getBody());

          // check user credentials and return fake jwt token if valid
          if (params.username === testAliment.value) {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: {     token: "fake-jwt-token",
                role: "eleveur",
                resources: [
                  {
                    name: "r1",
                    description: "des",
                    actions: [
                      "READ",
                      "WRITE"
                    ]
                  }
                ]
              } })
            ));
          } else {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200 })
            ));
          }
        }

        // fake users api end point
        if (connection.request.url.endsWith('/api/users') && connection.request.method === RequestMethod.Get) {
          // check for fake auth token in header and return test users if valid, this security is implemented server side
          // in a real application
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: [testUser] })
            ));
          } else {
            // return 401 not authorised if token is null or invalid
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 401 })
            ));
          }
        }

        // fake users api end point
        if (connection.request.url.endsWith('api/v1.0/eleveurs/elevages/mortalites') && connection.request.method === RequestMethod.Get) {
          // check for fake auth token in header and return test users if valid, this security is implemented server side
          // in a real application
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: [testMortalite] })
            ));
          } else {
            // return 401 not authorised if token is null or invalid
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 401 })
            ));
          }
        }

        // fake users api end point
        if (connection.request.url.endsWith('api/v1.0/eleveurs/elevages/aliments') && connection.request.method === RequestMethod.Get) {
          // check for fake auth token in header and return test users if valid, this security is implemented server side
          // in a real application
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: [testLivraisons] })
            ));
          } else {
            // return 401 not authorised if token is null or invalid
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 401 })
            ));
          }
        }

        // fake users api end point
        if (connection.request.url.endsWith('/api/v1.0/eleveurs/elevages/aliments/stock') && connection.request.method === RequestMethod.Get) {
          // check for fake auth token in header and return test users if valid, this security is implemented server side
          // in a real application
          if (connection.request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 200, body: testStock })
            ));
          } else {
            // return 401 not authorised if token is null or invalid
            connection.mockRespond(new Response(
              new ResponseOptions({ status: 401 })
            ));
          }
        }


      }, 500);

    });

    return new Http(backend, options);
  },
  deps: [MockBackend, BaseRequestOptions]
};
