import { Component, OnInit } from '@angular/core';

import { Mortalite } from './mortalite.model';
import { MortaliteService } from './mortalite.service';

@Component({
  selector: 'mortalite',
  templateUrl: './mortalite.component.html',
  styleUrls: [ './mortalite.component.css' ]
})
export class MortaliteComponent implements OnInit {

  model: Mortalite = new Mortalite();
  mortalites: Mortalite[] = [];
  dateMortalite: any;
  sexes = [
    {value: 0, viewValue: 'Mâle'},
    {value: 1, viewValue: 'Femelle'},
  ];

  constructor(private mortaliteService: MortaliteService) { }

  ngOnInit(): void {

    this.mortaliteService.getMortalite()
      .subscribe(mortalites => {
        this.mortalites = mortalites
        console.log(this.mortalites);
      });
  }

  addMortalite(): void {
    let newDate = this.dateMortalite.toString().replace(/\-/g, '');
    let date: number = parseInt(newDate);
    this.model.date = date;
    this.mortalites.push(this.model);
    this.mortaliteService.createMortalite(this.model)
      .subscribe(result => {
        this.model = new Mortalite();
        this.dateMortalite = "";
      });
  }
}
