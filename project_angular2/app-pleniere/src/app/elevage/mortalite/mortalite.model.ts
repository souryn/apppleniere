export class Mortalite {
  age: number;
  date: number;
  sexe: number;
  nbMort: number;
  obs: string;
}
