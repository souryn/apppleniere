import { Component, OnInit } from '@angular/core';

import { Stock } from './stock.model';
import { StockService } from './stock.service';

@Component({
  selector: 'stock',
  templateUrl: './stock.component.html',
  styleUrls: [ './stock.component.css' ]
})
export class StockComponent implements OnInit {

  model: Stock = new Stock();
  stock: Stock;
  constructor(private stockService: StockService) { }
  modelPrec: any = {};
  modelConsomme: any = {};
  modelRestant: any = {};

  ngOnInit(): void {
    this.stockService.getStock()
      .subscribe(stock => {
        this.stock = stock
        console.log(this.stock);
      });
  }

  getStock(): void {
    this.stockService.getStock()
      .subscribe(stock => {
        this.stock = stock
        console.log(this.stock);
      });
  }

  addPrec(): void {
    this.stockService.addAlimentPrecedent(this.modelPrec.value)
      .subscribe(result => {
        this.getStock();
      });
  }

  addConsomme(): void {
    this.stockService.addAlimentConsomme(this.modelConsomme.value)
      .subscribe(result => {
        this.getStock();
      });
  }

  addRestant(): void {
    this.stockService.addAlimentRestant(this.modelRestant.value)
      .subscribe(result => {
        this.getStock();
      });
  }
}
