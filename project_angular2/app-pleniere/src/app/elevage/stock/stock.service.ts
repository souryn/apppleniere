import {Stock} from './stock.model';
import { MORTALITES } from '../../mocks';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { Http, Response, URLSearchParams, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import {Observable} from 'rxjs';
import {LoginService} from "../../login/login.service";
import { API_ELEVEUR } from '../../constants/api.constants';

@Injectable()
export class StockService {

  resourceUrlStock = API_ELEVEUR + "/api/v1.0/eleveurs/elevages/aliments/stock";
  resourceUrlAddConsomme = API_ELEVEUR + "/api/v1.0/eleveurs/elevages/aliments/stock/consomme";
  resourceUrlAddRestant = API_ELEVEUR + "/api/v1.0/eleveurs/elevages/aliments/stock/restant";
  resourceUrlAddPrecedent = API_ELEVEUR + "/api/v1.0/eleveurs/elevages/aliments/stock/precedent";

  constructor(private http: Http, private loginService: LoginService) {
    // set token if saved in local storage
    /* var currentUser = JSON.parse(localStorage.getItem('token'));
     this.token = currentUser && currentUser.token;*/
  }



  getStock(): Observable<Stock> {
    let headers = new Headers({'Authorization': 'Bearer ' + this.loginService.token});
    headers.append('content-type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.resourceUrlStock, options)
      .map((response: Response) => response.json());
  }

  addAlimentConsomme(value: number): Observable<Response> {
    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.loginService.token });
    headers.append('content-type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.resourceUrlAddConsomme, JSON.stringify({ value: value }), options).map((res: Response) => {
      return res.json();
    });
  }

  addAlimentRestant(value: number): Observable<Response> {

    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.loginService.token });
    headers.append('content-type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.resourceUrlAddRestant, JSON.stringify({ value: value }), options).map((res: Response) => {
      return res.json();
    });
  }

  addAlimentPrecedent(value: number): Observable<Response> {

    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.loginService.token });
    headers.append('content-type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this.http.post(this.resourceUrlAddPrecedent, JSON.stringify({ value: value }), options).map((res: Response) => {
      return res.json();
    });
  }
}
