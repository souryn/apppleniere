import { Component, OnInit } from '@angular/core';

import { Livraison } from './livraison.model';
import { LivraisonService } from './livraison.service';

@Component({
  selector: 'livraison',
  templateUrl: './livraison.component.html',
  styleUrls: [ './livraison.component.css' ]
})
export class LivraisonComponent implements OnInit {

  model: Livraison = new Livraison();
  livraisons: Livraison[] = [];
  dateLivraison: any;
  sexes = [
    {value: 0, viewValue: 'Mâle'},
    {value: 1, viewValue: 'Femelle'},
  ];

  constructor(private livraisonService: LivraisonService) { }

  ngOnInit(): void {
    this.livraisonService.getLivraison()
      .subscribe(livraisons => {
        this.livraisons = livraisons
        console.log(this.livraisons);
      });
  }

  addLivraison(): void { /*add Livraison */
    let newDate = this.dateLivraison.toString().replace(/\-/g, '');
    let date: number = parseInt(newDate);
    this.model.date = date;
    this.livraisons.push(this.model);
    this.livraisonService.createLivraison(this.model)
      .subscribe(result => {
        this.model = new Livraison();
        this.dateLivraison = "";
      });
  }
}
