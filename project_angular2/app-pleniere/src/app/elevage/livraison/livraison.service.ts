import { Livraison } from './livraison.model';
import { MORTALITES } from '../../mocks';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { Http, Response, URLSearchParams, BaseRequestOptions, RequestOptions, Headers } from '@angular/http';
import {Observable} from 'rxjs';
import {LoginService} from "../../login/login.service";
import { API_ELEVEUR } from '../../constants/api.constants';

@Injectable()
export class LivraisonService {

  resourceUrl = API_ELEVEUR + "/api/v1.0/eleveurs/elevages/aliments";

  constructor(private http: Http, private loginService: LoginService) {
    // set token if saved in local storage
    /* var currentUser = JSON.parse(localStorage.getItem('token'));
     this.token = currentUser && currentUser.token;*/
  }



  getLivraison(): Observable<Livraison[]> {
    let headers = new Headers({'Authorization': 'Bearer ' + this.loginService.token});
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.resourceUrl, options)
      .map((response: Response) => response.json());
  }

  createLivraison(livraison: Livraison): Observable<Livraison> {

    // add authorization header with jwt token
    let headers = new Headers({ 'Authorization': 'Bearer ' + this.loginService.token });
    let options = new RequestOptions({ headers: headers });

    let copy: Livraison = Object.assign({}, livraison);
    console.log(copy);
    return this.http.post(this.resourceUrl, copy).map((res: Response) => {
      return res.json();
    });
  }

  /*getHeroesSlowly(): Promise<Hero[]> {
    return new Promise(resolve => {
      // Simulate server latency with 2 second delay
      setTimeout(() => resolve(this.getHeroes()), 2000);
    });
  }

  getHero(id: number): Promise<Hero> {
    return this.getHeroes()
               .then(heroes => heroes.find(hero => hero.id === id));
  }*/
}
