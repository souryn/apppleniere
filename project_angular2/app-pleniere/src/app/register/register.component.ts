import 'rxjs/add/operator/switchMap';
import { Component, OnInit }        from '@angular/core';
import {Router} from "@angular/router";
import {RegisterService} from './register.service';
import {MdSnackBar} from "@angular/material";

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: [ './register.css' ]
})
export class RegisterComponent implements OnInit {

  model: any = {};
  roles = [
    {value: 'eleveur', viewValue: 'Eleveur'},

  ];
  isLoading: boolean = false;
  isSuccess: boolean = false;
  isError: boolean = false;

  constructor(private registerService: RegisterService, private router: Router, private _snackbar: MdSnackBar) {}

  ngOnInit(): void {
  }
  register(): void {
    this.isLoading = true;
    this.registerService.register(this.model.username, this.model.password, this.model.role)
      .subscribe(
        result => {
          this.isLoading = false;
          this._snackbar.open('YUM SNACKS', 'CHEW');
          this.router.navigate(['login']);
      },
        err => {
          this.isLoading = false;
          this._snackbar.open('Inscription', 'L\'inscription a échouée', {
            duration: 3000
          });
          console.log(err)
      },
        () => {
        this.isLoading = false;
      });
  }
}
