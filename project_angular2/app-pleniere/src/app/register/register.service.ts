import { Injectable } from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { API_ACCOUNT } from '../constants/api.constants';

@Injectable()
export class RegisterService {

  public token: string;

  resourceLogin = API_ACCOUNT + "/api/v1.0/souscription";
  constructor(private http: Http) {
  }

  register(username: string, password: string, role: string): Observable<Response> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log(JSON.stringify({ login: username,  role: role, password: password}));
    return this.http.post(this.resourceLogin, JSON.stringify({ login: username,  role: role, password: password}), options)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let token = response.json() && response.json().token;
        if (token) {
          // set token property
          this.token = token;

          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', token);

          // return true to indicate successful login
          return response.json();
        } else {
          // return false to indicate failed login
          return false;
        }
      });
  }
}
