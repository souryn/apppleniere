import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { MortaliteComponent } from './elevage/mortalite';
import { LivraisonComponent } from './elevage/livraison';
import { StockComponent } from './elevage/stock';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'mortalite',  component: MortaliteComponent, canActivate: [AuthGuard] },
  { path: 'livraison',  component: LivraisonComponent, canActivate: [AuthGuard] },
  { path: 'stock',  component: StockComponent, canActivate: [AuthGuard] },
  /*{ path: 'detail/:id', component: HeroDetailComponent },*/
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
