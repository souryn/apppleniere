import 'rxjs/add/operator/switchMap';
import { Component, OnInit }        from '@angular/core';
import { LoginService } from "../../login";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: [ './navbar.css' ]
})
export class NavbarComponent implements OnInit {

  constructor(private loginService: LoginService) {}

  ngOnInit(): void {
  }

  logout(): void {
    this.loginService.logout();
  }

}
