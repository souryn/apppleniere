import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule }     from './app-routing.module';
import { SidebarComponent } from './layout/sidebar';
import { LoginComponent } from './login';
// used to create fake backend
import { BaseRequestOptions } from '@angular/http';
import { LoginService } from './login';
import { RegisterComponent } from  './register';
import { RegisterService } from "./register/register.service";
import { MortaliteComponent } from './elevage/mortalite/mortalite.component';
import { MortaliteService } from './elevage/mortalite/mortalite.service';
import { LivraisonComponent } from './elevage/livraison/livraison.component';
import { LivraisonService } from './elevage/livraison/livraison.service';
import { StockComponent } from './elevage/stock/stock.component';
import { StockService } from './elevage/stock/stock.service';
import { NavbarComponent } from './layout/navbar/navbar.component';
import {AuthGuard} from "./guards/auth.guard";

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    MortaliteComponent,
    LivraisonComponent,
    StockComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [
    BaseRequestOptions,
    LoginService,
    RegisterService,
    MortaliteService,
    LivraisonService,
    StockService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
